package com.example.demo1;

import java.util.HashMap;
import java.util.Map;
//предназначен для взаимодействия с БД, в данном классе представлена ввиде хэш мапе
public class DataDAO {

    //Имитация базы данных
   private static final Map<String, UserAccount> mapUsers = new HashMap<String, UserAccount>();

   // блок иннициализации, который добавлят новых пользователей в нашу "БД"
    {
      UserAccount emp = new UserAccount("employee1", "123", UserAccount.GENDER_MALE, //
            SecurityConfig.ROLE_EMPLOYEE);
      UserAccount mng = new UserAccount("manager1", "123", UserAccount.GENDER_MALE, //
            SecurityConfig.ROLE_EMPLOYEE, SecurityConfig.ROLE_MANAGER);
      mapUsers.put(emp.getUserName(), emp);
      mapUsers.put(mng.getUserName(), mng);
   }
 //проверка корректности ввода данных
   public static UserAccount findUser(String userName, String password) {
      UserAccount u = mapUsers.get(userName);
      if (u != null && u.getPassword().equals(password)) {
         return u;
      }
      return null;
   }

}