package com.kharitonov.Lab7.controller;

import com.kharitonov.Lab7.domain.Book;
import com.kharitonov.Lab7.repository.BookRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * The type Book controller.
 */
@RestController
@RequestMapping("/book")
public class BookController {

    private final BookRepository bookRepository;

    /**
     * Instantiates a new Book controller.
     *
     * @param bookRepository the book repository
     */
    @Autowired
    public BookController(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    /**
     * All book list.
     *
     * @return the list
     */
    @GetMapping
    public List<Book> allBook() {
        return bookRepository.findAll();
    }

    /**
     * Find by id book book.
     *
     * @param book the book
     * @return the book
     */
    @GetMapping("{id}")
    public Book findByIdBook(@PathVariable("id") Book book) {
        return book;
    }

    /**
     * Create book book.
     *
     * @param book the book
     * @return the book
     */
    @PostMapping
    public Book createBook(@RequestBody Book book) {
        return bookRepository.save(book);
    }

    /**
     * Update book book.
     *
     * @param bookFromDB the book from db
     * @param book       the book
     * @return the book
     */
    @PutMapping("{id}")
    public Book updateBook(@PathVariable("id") Book bookFromDB,
                           @RequestBody Book book) {
        BeanUtils.copyProperties(book, bookFromDB, "id");
        return bookRepository.save(bookFromDB);

    }

    /**
     * Delete book.
     *
     * @param book the book
     */
    @DeleteMapping("{id}")
    public void deleteBook(@PathVariable("id") Book book) {
        bookRepository.delete(book);
    }

}
