package com.kharitonov.Lab7.domain;

import lombok.Data;

import javax.persistence.*;

/**
 * The type Book.
 */
@Entity
@Data
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private String author;
    private Integer countPages;
}
