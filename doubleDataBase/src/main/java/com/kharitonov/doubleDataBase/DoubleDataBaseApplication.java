package com.kharitonov.doubleDataBase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude =  {DataSourceAutoConfiguration.class })
public class DoubleDataBaseApplication {

	public static void main(String[] args) {
		SpringApplication.run(DoubleDataBaseApplication.class, args);
	}

}
