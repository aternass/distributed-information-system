package com.kharitonov.doubleDataBase.controller;

import com.kharitonov.doubleDataBase.entity.Student;
import com.kharitonov.doubleDataBase.service.StudentOtherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/studentOther")
public class StudentOtherController {

    private StudentOtherService studentOtherService;

    @Autowired
    public StudentOtherController(StudentOtherService studentOtherService) {
        this.studentOtherService = studentOtherService;
    }

    @GetMapping("/{id}")
    public Student getStudentById(@PathVariable("id") Long id) {
        return studentOtherService.findById(id);
    }

    @DeleteMapping("/{id}")
    public void deleteStudent(@PathVariable("id") Student student) {
        studentOtherService.delete(student);
    }
}
