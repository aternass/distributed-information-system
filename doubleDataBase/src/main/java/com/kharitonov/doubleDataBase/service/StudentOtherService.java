package com.kharitonov.doubleDataBase.service;

import com.kharitonov.doubleDataBase.entity.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentOtherService {

    private StudentOtherService studentOtherService;

    @Autowired
    public StudentOtherService(StudentOtherService studentOtherService) {
        this.studentOtherService = studentOtherService;
    }

    public Student save(Student student) {
        return studentOtherService.save(student);
    }

    public List<Student> findAll() {
        return studentOtherService.findAll();
    }

    public void delete(Student student) {
        studentOtherService.delete(student);
    }

    public Student findById(Long id) {
        return studentOtherService.findById(id);
    }
}
